/**
 * Created by Dmitry Vereykin on 7/27/2015.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;

public class ShadeDesigner extends JFrame {
    private StylesPanel styles;
    private QuantityPanel quantity;
    private JPanel downPanel;
    private SizesPanel sizes;
    private ColorsPanel colors;
    private TopBanner topBanner;
    private JPanel buttonPanel;
    private JButton calcButton;
    private final double TAX_RATE = 0.06;

    private JTextField subTotalField;
    private JTextField taxField;
    private JTextField totalField;

    public ShadeDesigner() {
        setTitle("Shade designer");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        topBanner = new TopBanner();
        styles = new StylesPanel();
        quantity = new QuantityPanel();
        sizes = new SizesPanel();
        colors = new ColorsPanel();
        downPanel = new JPanel();

        buildDownPanel();

        add(topBanner, BorderLayout.NORTH);
        add(styles, BorderLayout.WEST);
        add(colors, BorderLayout.EAST);
        add(sizes, BorderLayout.CENTER);
        add(downPanel, BorderLayout.SOUTH);

        pack();
        setVisible(true);
        setLocationRelativeTo(null);
    }

    private void buildDownPanel() {
        downPanel = new JPanel();
        downPanel.setLayout(new GridLayout(1, 2));

        buildButtonPanel();

        downPanel.add(quantity);
        downPanel.add(buttonPanel);
    }


    private void buildButtonPanel() {
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(4, 2));

        calcButton = new JButton("Calculate");
        calcButton.addActionListener(new CalcButtonListener());

        subTotalField = new JTextField(5);
        subTotalField.setHorizontalAlignment(JTextField.RIGHT);
        subTotalField.setEditable(false);

        taxField = new JTextField(5);
        taxField.setHorizontalAlignment(JTextField.RIGHT);
        taxField.setEditable(false);

        totalField = new JTextField(5);
        totalField.setHorizontalAlignment(JTextField.RIGHT);
        totalField.setEditable(false);

        buttonPanel.add(new JLabel("Subtotal: "));
        buttonPanel.add(subTotalField);

        buttonPanel.add(new JLabel("Tax: "));
        buttonPanel.add(taxField);

        buttonPanel.add(new JLabel("Total: "));
        buttonPanel.add(totalField);

        buttonPanel.add(calcButton);
        buttonPanel.add(new JLabel("      Thank you  "));
    }


    private class CalcButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            double subtotal, tax, total;

            subtotal = (50 + styles.getStyleCost() +
                    sizes.getSizeCost() +
                    colors.getColorCost()) * quantity.getQuantity();

            tax = subtotal * TAX_RATE;
            total = subtotal + tax;

            DecimalFormat dollar = new DecimalFormat("0.00");

            subTotalField.setText(dollar.format(subtotal));
            taxField.setText(dollar.format(tax));
            totalField.setText("$" + dollar.format(total));
        }
    }

    public static void main(String[] args) {
        new ShadeDesigner();
    }
}