/**
 * Created by Dmitry Vereykin on 7/27/2015.
 */
import javax.swing.*;
import java.awt.*;

public class QuantityPanel extends JPanel {

    private JTextField number;

    public QuantityPanel() {
        setLayout(new GridLayout(2, 1));

        JLabel label = new JLabel("Type the desirable number:");
        number = new JTextField("0", 5);
        setBorder(BorderFactory.createTitledBorder("Quantity"));

        add(label);
        add(number);

    }

    public double getQuantity() {
        int quantity = 0;

        try {
            quantity = Integer.parseInt(number.getText());
        } catch (Exception exc) {
            JOptionPane.showMessageDialog(null, "ERROR. Entered value is not a number.");
        }

        if (quantity < 0) {
            quantity = 0;
            JOptionPane.showMessageDialog(null, "ERROR. Entered value is negative.");
        }

        return quantity;
    }
}