/**
 * Created by Dmitry Vereykin on 7/27/2015.
 */
import javax.swing.*;
import java.awt.*;

public class StylesPanel extends JPanel {
    public final double REGULAR = 0;
    public final double FOLDING = 10;
    public final double ROMAN = 15;

    private JRadioButton regular;
    private JRadioButton folding;
    private JRadioButton roman;
    private ButtonGroup bg;

    public StylesPanel() {
        setLayout(new GridLayout(3, 1));

        regular = new JRadioButton("Regular shades", true);
        folding = new JRadioButton("Folding shades");
        roman = new JRadioButton("Roman shades");

        bg = new ButtonGroup();
        bg.add(regular);
        bg.add(folding);
        bg.add(roman);

        setBorder(BorderFactory.createTitledBorder("Styles"));

        add(regular);
        add(folding);
        add(roman);
    }

    public double getStyleCost() {
        double styleCost;

        if (regular.isSelected())
            styleCost = REGULAR;
        else if (folding.isSelected())
            styleCost = FOLDING;
        else
            styleCost = ROMAN;
   
      return styleCost;
   }
}